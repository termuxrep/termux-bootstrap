#!/bin/bash

NAME="dev-boot"

function generate_url() {
    manager="$1"
    arch="$2"
    if [ "$manager-$arch" = "pacman-arm"]; then
        curl -OL "https://www.dropbox.com/scl/fi/nnnlw92u6qntsesq4zdg8/pacman-bootstrap-arm.zip?rlkey=ysa6t1c0dcmkiy3xe6qkbxsjv&dl=0" &>/dev/null
    elif [ "$manager-$arch" = "apt-arm" ]; then
        curl -OL "https://www.dropbox.com/scl/fi/qtkz77z7e21p4z8zcyphx/apt-bootstrap-arm.zip?rlkey=hn9qlvr8sdbtrwj8vcfcrl90l&dl=0" &>/dev/null
    else
        echo "[*] Invalid '$manager' or '$arch'"
        exit 0
}

if [ "$1" = "--manager" ]; then
    check="True"
    echo "$check"
    manager="$2"
    if [ "$3" = "--arch" ]; then
        check="True"
        echo "$check"
        arch="$4"
        generate_url "$2" "$4"
    else
        check="False"
        echo "$check"
        exit 0
    fi
else
    check="False"
    echo "$check"
    exit 0
fi